# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2023 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed LGPL-3.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

import json
from datetime import datetime

from sqlalchemy import and_
from werkzeug.datastructures.structures import MultiDict

from apps.forms.dept import DeptForm
from apps.models.dept import Dept
from extends import db
from utils import R, regular

from utils.utils import uid


# 查询部门数据
async def DeptList(self):
    try:
        # 实例化查询对象
        query = db.query(Dept).filter(Dept.is_delete == 0)
        # 部门名称
        name = self.get_query_argument('name', None)
        if name:
            # 部门名称模糊查询
            query = query.filter(Dept.name.like('%' + name + '%'))
        # 查询数据
        list = query.order_by(Dept.sort.asc()).all()
        # 实例化数组对象
        result = []
        # 遍历数据源
        if list:
            for item in list:
                # 对象转字典
                data = item.to_dict()
                # 加入列表
                result.append(data)
        # 返回结果
        return R.ok(self, data=result)
    except:
        # 抛出异常
        raise
    finally:
        # 关闭连接
        db.close()


# 根据ID查询部门
async def DeptDetail(dept_id):
    try:
        # 根据ID查询部门
        dept = db.query(Dept).filter(and_(Dept.id == dept_id, Dept.is_delete == 0)).first()
        # 查询结果判空
        if not dept:
            return None
        # 对象转字典
        data = dept.to_dict()
        # 返回结果
        return data
    except:
        # 抛出异常
        raise
    finally:
        # 关闭连接
        db.close()


# 添加部门
async def DeptAdd(self):
    # 获取请求参数
    json_data = json.loads(self.request.body)
    # 表单验证
    form = DeptForm(MultiDict(json_data))
    if not form.validate():
        # 获取错误描述
        err_msg = regular.get_err(form)
        # 返回错误信息
        return R.failed(self, msg=err_msg)

    # 表单数据赋值给对象
    dept = Dept(**form.data)
    dept.create_user = uid(self)
    # 插入数据
    dept.save()
    # 返回结果
    return R.ok(self, msg="添加成功")


# 更新部门
async def DeptUpdate(self):
    # 获取请求参数
    json_data = json.loads(self.request.body)
    # 表单验证
    form = DeptForm(MultiDict(json_data))
    if not form.validate():
        # 获取错误描述
        err_msg = regular.get_err(form)
        # 返回错误信息
        return R.failed(self, msg=err_msg)

    # 记录ID判空
    id = form.data['id']
    if not id or int(id) <= 0:
        return R.failed(self, "记录ID不能为空")

    try:
        # 根据ID查询记录
        dept = db.query(Dept).filter(and_(Dept.id == id, Dept.is_delete == 0)).first()
        # 查询结果判空
        if not dept:
            return R.failed(self, "记录不存在")
        # 删除ID元素
        del form['id']
        updData = form.data
        updData['update_user'] = uid(self)
        updData['update_time'] = datetime.now()
        result = db.query(Dept).filter_by(id=id).update(updData)
        # 提交数据
        db.commit()
        if not result:
            return R.failed(self, "更新失败")
        # 返回结果
        return R.ok(self, msg="更新成功")
    except:
        # 事务回滚
        db.rollback()
        raise
    finally:
        # 关闭连接
        db.close()


# 删除部门
async def DeptDelete(self, dept_id):
    # 记录ID为空判断
    if not dept_id:
        return R.failed(self, "记录ID不存在")
    try:
        # 分裂字符串
        list = dept_id.split(',')
        # 计数器
        count = 0
        # 遍历数据源
        if len(list) > 0:
            for vId in list:
                # 根据ID查询记录
                dept = db.query(Dept).filter(and_(Dept.id == int(vId), Dept.is_delete == 0)).first()
                # 查询结果判空
                if not dept:
                    return R.failed(self, "记录不存在")
                # 设置删除标识
                dept.is_delete = 1
                # 提交数据
                db.commit()
                # 计数器+1
                count += 1
        # 返回结果
        return R.ok(self, msg="本次共删除{0}条数据".format(count))
    except:
        # 事务回滚
        db.rollback()
        raise
    finally:
        # 关闭连接
        db.close()


# 获取部门列表
async def getDeptList():
    try:
        # 查询部门数据列表
        list = db.query(Dept).filter(Dept.is_delete == 0).order_by(Dept.sort.asc()).all()
        # 实例化列表
        dept_list = []
        # 遍历数据源
        if list:
            for v in list:
                # 对象转字典
                item = v.to_dict()
                # 加入数组
                dept_list.append(item)
        # 返回结果
        return dept_list
    except:
        # 抛出异常
        raise
    finally:
        # 关闭连接
        db.close()
