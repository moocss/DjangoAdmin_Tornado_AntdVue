# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2023 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed LGPL-3.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

from apps.views.ad import AdListHandler, AdDetailHandler, AdAddHandler, AdUpdateHandler, \
    AdDeleteHandler, AdStatusHandler
from apps.views.ad_sort import AdSortListHandler, AdSortDetailHandler, AdSortAddHandler, \
    AdSortUpdateHandler, AdSortDeleteHandler, AdSortGetListHandler
from apps.views.city import CityListHandler, CityDetailHandler, CityAddHandler, CityUpdateHandler, \
    CityDeleteHandler
from apps.views.config import ConfigListHandler, ConfigDetailHandler, ConfigAddHandler, \
    ConfigUpdateHandler, ConfigDeleteHandler
from apps.views.config_data import ConfigDataListHandler, ConfigDataDetailHandler, \
    ConfigDataAddHandler, ConfigDataUpdateHandler, ConfigDataDeleteHandler, ConfigDataStatusHandler
from apps.views.config_web import ConfigWebIndexHandler, ConfigWebSaveHandler
from apps.views.dept import DeptListHandler, DeptDetailHandler, DeptAddHandler, DeptUpdateHandler, \
    DeptDeleteHandler, DeptGetListHandler
from apps.views.dicts import DictListHandler, DictDetailHandler, DictAddHandler, DictUpdateHandler, \
    DictDeleteHandler
from apps.views.dicts_data import DictDataListHandler, DictDataDetailHandler, DictDataAddHandler, \
    DictDataUpdateHandler, DictDataDeleteHandler
from apps.views.index import UserHandler, UpdatePwdHandler, LogoutHandler, MenuHandler, UpdateUserHandler
from apps.views.item import ItemListHandler, ItemDetailHandler, ItemAddHandler, ItemUpdateHandler, \
    ItemDeleteHandler, ItemGetListHandler
from apps.views.item_cate import ItemCateListHandler, ItemCateDetailHandler, ItemCateAddHandler, \
    ItemCateUpdateHandler, ItemCateDeleteHandler, ItemCateGetListHandler
from apps.views.level import LevelListHandler, LevelDetailHandler, LevelAddHandler, \
    LevelUpdateHandler, LevelDeleteHandler, LevelStatusHandler, LevelImportHandler, LevelExportHandler, \
    LevelGetListHandler
from apps.views.link import LinkListHandler, LinkDetailHandler, LinkAddHandler, LinkUpdateHandler, \
    LinkDeleteHandler, LinkStatusHandler
from apps.views.login import LoginHandler, CaptchaHandler
from apps.views.member import MemberListHandler, MemberDetailHandler, MemberAddHandler, \
    MemberUpdateHandler, MemberDeleteHandler, MemberStatusHandler
from apps.views.member_level import MemberLevelListHandler, MemberLevelDetailHandler, MemberLevelAddHandler, \
    MemberLevelUpdateHandler, MemberLevelDeleteHandler, MemberLevelGetListHandler
from apps.views.menu import MenuListHandler, MenuDetailHandler, MenuAddHandler, MenuUpdateHandler, \
    MenuDeleteHandler
from apps.views.notice import NoticeListHandler, NoticeDetailHandler, NoticeAddHandler, NoticeUpdateHandler, \
    NoticeDeleteHandler

from apps.views.position import PositionListHandler, PositionDetailHandler, PositionAddHandler, PositionUpdateHandler, \
    PositionDeleteHandler, PositionStatusHandler, PositionGetListHandler

from apps.views.role import RoleListHandler, RoleDetailHandler, RoleAddHandler, RoleUpdateHandler, \
    RoleDeleteHandler, RoleStatusHandler, RoleGetListHandler
from apps.views.role_menu import RoleMenuIndexHandler, RoleMenuSaveHandler
from apps.views.upload import UploadHandler, UploadEditHandler
from apps.views.user import UserListHandler, UserDetailHandler, UserAddHandler, UserUpdateHandler, \
    UserDeleteHandler, UserStatusHandler, UserResetPwdHandler

# 定义路由集合
router = list()


# 登录路由
def login_router():
    # 路由配置
    r = [
        (r'/login', LoginHandler),
        (r'/captcha', CaptchaHandler),
    ]
    # 路由前缀
    login_r = ""
    # 路由数组
    login = [(login_r + v[0], v[1]) for v in r]
    print('登录路由：', login)
    # 返回路由
    return login


# 主页路由
def index_router():
    # 路由配置
    r = [
        (r'/menu', MenuHandler),
        (r'/user', UserHandler),
        (r'/userInfo', UpdateUserHandler),
        (r'/updatePwd', UpdatePwdHandler),
        (r'/logout', LogoutHandler),
    ]
    # 路由前缀
    index_r = "/index"
    # 路由数组
    index = [(index_r + v[0], v[1]) for v in r]
    print('主页路由：', index)
    # 返回路由
    return index


# 上传文件路由
def upload_router():
    # 路由配置
    r = [
        (r'/uploadImage', UploadHandler),
        (r'/uploadEditImage', UploadEditHandler),
    ]
    # 路由前缀
    upload_r = "/upload"
    # 路由数组
    upload = [(upload_r + v[0], v[1]) for v in r]
    print('上传文件路由：', upload)
    # 返回路由
    return upload


# 职级路由
def level_router():
    # 路由配置
    r = [
        (r'/list?', LevelListHandler),
        (r'/detail/(\d+)', LevelDetailHandler),
        (r'/add', LevelAddHandler),
        (r'/update', LevelUpdateHandler),
        (r'/delete/(\d+)', LevelDeleteHandler),
        (r'/status', LevelStatusHandler),
        (r'/import', LevelImportHandler),
        (r'/export', LevelExportHandler),
        (r'/getLevelList', LevelGetListHandler),
    ]
    # 路由前缀
    level_r = "/level"
    # 路由数组
    level = [(level_r + v[0], v[1]) for v in r]
    print('职级路由：', level)
    # 返回路由
    return level


# 岗位路由
def position_router():
    # 路由配置
    r = [
        (r'/list?', PositionListHandler),
        (r'/detail/(\d+)', PositionDetailHandler),
        (r'/add', PositionAddHandler),
        (r'/update', PositionUpdateHandler),
        (r'/delete/(\d+)', PositionDeleteHandler),
        (r'/status', PositionStatusHandler),
        (r'/getPositionList', PositionGetListHandler),
    ]
    # 路由前缀
    position_r = "/position"
    # 路由数组
    position = [(position_r + v[0], v[1]) for v in r]
    print('岗位路由：', position)
    # 返回路由
    return position


# 部门路由
def dept_router():
    # 路由配置
    r = [
        (r'/list?', DeptListHandler),
        (r'/detail/(\d+)', DeptDetailHandler),
        (r'/add', DeptAddHandler),
        (r'/update', DeptUpdateHandler),
        (r'/delete/(\d+)', DeptDeleteHandler),
        (r'/getDeptList', DeptGetListHandler),
    ]
    # 路由前缀
    dept_r = "/dept"
    # 路由数组
    dept = [(dept_r + v[0], v[1]) for v in r]
    print('部门路由：', dept)
    # 返回路由
    return dept


# 角色路由
def role_router():
    # 路由配置
    r = [
        (r'/list?', RoleListHandler),
        (r'/detail/(\d+)', RoleDetailHandler),
        (r'/add', RoleAddHandler),
        (r'/update', RoleUpdateHandler),
        (r'/delete/(\d+)', RoleDeleteHandler),
        (r'/status', RoleStatusHandler),
        (r'/getRoleList', RoleGetListHandler),
    ]
    # 路由前缀
    role_r = "/role"
    # 路由数组
    role = [(role_r + v[0], v[1]) for v in r]
    print('角色路由：', role)
    # 返回路由
    return role


# 角色菜单路由
def role_menu_router():
    # 路由配置
    r = [
        (r'/index/(\d+)', RoleMenuIndexHandler),
        (r'/save', RoleMenuSaveHandler)
    ]
    # 路由前缀
    role_menu_r = "/rolemenu"
    # 路由数组
    role_menu = [(role_menu_r + v[0], v[1]) for v in r]
    print('角色菜单路由：', role_menu)
    # 返回路由
    return role_menu


# 菜单路由
def menu_router():
    # 路由配置
    r = [
        (r'/list?', MenuListHandler),
        (r'/detail/(\d+)', MenuDetailHandler),
        (r'/add', MenuAddHandler),
        (r'/update', MenuUpdateHandler),
        (r'/delete/(\d+)', MenuDeleteHandler),
    ]
    # 路由前缀
    menu_r = "/menu"
    # 路由数组
    menu = [(menu_r + v[0], v[1]) for v in r]
    print('角色路由：', menu)
    # 返回路由
    return menu


# 用户路由
def user_router():
    # 路由配置
    r = [
        (r'/list?', UserListHandler),
        (r'/detail/(\d+)', UserDetailHandler),
        (r'/add', UserAddHandler),
        (r'/update', UserUpdateHandler),
        (r'/delete/(\d+)', UserDeleteHandler),
        (r'/status', UserStatusHandler),
        (r'/resetPwd', UserResetPwdHandler),
    ]
    # 路由前缀
    user_r = "/user"
    # 路由数组
    user = [(user_r + v[0], v[1]) for v in r]
    print('用户路由：', user)
    # 返回路由
    return user


# 用户路由
def city_router():
    # 路由配置
    r = [
        (r'/list?', CityListHandler),
        (r'/detail/(\d+)', CityDetailHandler),
        (r'/add', CityAddHandler),
        (r'/update', CityUpdateHandler),
        (r'/delete/(\d+)', CityDeleteHandler),
    ]
    # 路由前缀
    city_r = "/city"
    # 路由数组
    city = [(city_r + v[0], v[1]) for v in r]
    print('城市路由：', city)
    # 返回路由
    return city


# 通知路由
def notice_router():
    # 路由配置
    r = [
        (r'/list?', NoticeListHandler),
        (r'/detail/(\d+)', NoticeDetailHandler),
        (r'/add', NoticeAddHandler),
        (r'/update', NoticeUpdateHandler),
        (r'/delete/(\d+)', NoticeDeleteHandler),
    ]
    # 路由前缀
    notice_r = "/notice"
    # 路由数组
    notice = [(notice_r + v[0], v[1]) for v in r]
    print('通知路由：', notice)
    # 返回路由
    return notice


# 站点路由
def item_router():
    # 路由配置
    r = [
        (r'/list?', ItemListHandler),
        (r'/detail/(\d+)', ItemDetailHandler),
        (r'/add', ItemAddHandler),
        (r'/update', ItemUpdateHandler),
        (r'/delete/(\d+)', ItemDeleteHandler),
        (r'/getItemList', ItemGetListHandler),
    ]
    # 路由前缀
    item_r = "/item"
    # 路由数组
    item = [(item_r + v[0], v[1]) for v in r]
    print('站点路由：', item)
    # 返回路由
    return item


# 站点路由
def item_cate_router():
    # 路由配置
    r = [
        (r'/list?', ItemCateListHandler),
        (r'/detail/(\d+)', ItemCateDetailHandler),
        (r'/add', ItemCateAddHandler),
        (r'/update', ItemCateUpdateHandler),
        (r'/delete/(\d+)', ItemCateDeleteHandler),
        (r'/getCateList', ItemCateGetListHandler),
    ]
    # 路由前缀
    item_cate_r = "/itemcate"
    # 路由数组
    item_cate = [(item_cate_r + v[0], v[1]) for v in r]
    print('栏目路由：', item_cate)
    # 返回路由
    return item_cate


# 友链路由
def link_router():
    # 路由配置
    r = [
        (r'/list?', LinkListHandler),
        (r'/detail/(\d+)', LinkDetailHandler),
        (r'/add', LinkAddHandler),
        (r'/update', LinkUpdateHandler),
        (r'/delete/(\d+)', LinkDeleteHandler),
        (r'/status', LinkStatusHandler),
    ]
    # 路由前缀
    link_r = "/link"
    # 路由数组
    link = [(link_r + v[0], v[1]) for v in r]
    print('友链路由：', link)
    # 返回路由
    return link


# 广告位路由
def ad_sort_router():
    # 路由配置
    r = [
        (r'/list?', AdSortListHandler),
        (r'/detail/(\d+)', AdSortDetailHandler),
        (r'/add', AdSortAddHandler),
        (r'/update', AdSortUpdateHandler),
        (r'/delete/(\d+)', AdSortDeleteHandler),
        (r'/getAdSortList', AdSortGetListHandler),
    ]
    # 路由前缀
    ad_sort_r = "/adsort"
    # 路由数组
    ad_sort = [(ad_sort_r + v[0], v[1]) for v in r]
    print('广告位路由：', ad_sort)
    # 返回路由
    return ad_sort


# 广告路由
def ad_router():
    # 路由配置
    r = [
        (r'/list?', AdListHandler),
        (r'/detail/(\d+)', AdDetailHandler),
        (r'/add', AdAddHandler),
        (r'/update', AdUpdateHandler),
        (r'/delete/(\d+)', AdDeleteHandler),
        (r'/status', AdStatusHandler),
    ]
    # 路由前缀
    ad_r = "/ad"
    # 路由数组
    ad = [(ad_r + v[0], v[1]) for v in r]
    print('广告路由：', ad)
    # 返回路由
    return ad


# 会员等级路由
def member_level_router():
    # 路由配置
    r = [
        (r'/list?', MemberLevelListHandler),
        (r'/detail/(\d+)', MemberLevelDetailHandler),
        (r'/add', MemberLevelAddHandler),
        (r'/update', MemberLevelUpdateHandler),
        (r'/delete/(\d+)', MemberLevelDeleteHandler),
        (r'/getMemberLevelList', MemberLevelGetListHandler),
    ]
    # 路由前缀
    member_level_r = "/memberlevel"
    # 路由数组
    member_level = [(member_level_r + v[0], v[1]) for v in r]
    print('会员等级路由：', member_level)
    # 返回路由
    return member_level


# 会员路由
def member_router():
    # 路由配置
    r = [
        (r'/list?', MemberListHandler),
        (r'/detail/(\d+)', MemberDetailHandler),
        (r'/add', MemberAddHandler),
        (r'/update', MemberUpdateHandler),
        (r'/delete/(\d+)', MemberDeleteHandler),
        (r'/status', MemberStatusHandler),
    ]
    # 路由前缀
    member_r = "/member"
    # 路由数组
    member = [(member_r + v[0], v[1]) for v in r]
    print('会员路由：', member)
    # 返回路由
    return member


# 字典路由
def dict_router():
    # 路由配置
    r = [
        (r'/list?', DictListHandler),
        (r'/detail/(\d+)', DictDetailHandler),
        (r'/add', DictAddHandler),
        (r'/update', DictUpdateHandler),
        (r'/delete/(\d+)', DictDeleteHandler),
    ]
    # 路由前缀
    dict_r = "/dict"
    # 路由数组
    dict = [(dict_r + v[0], v[1]) for v in r]
    print('字典路由：', dict)
    # 返回路由
    return dict


# 字典数据路由
def dict_data_router():
    # 路由配置
    r = [
        (r'/list?', DictDataListHandler),
        (r'/detail/(\d+)', DictDataDetailHandler),
        (r'/add', DictDataAddHandler),
        (r'/update', DictDataUpdateHandler),
        (r'/delete/(\d+)', DictDataDeleteHandler),
    ]
    # 路由前缀
    dict_data_r = "/dictdata"
    # 路由数组
    dict_data = [(dict_data_r + v[0], v[1]) for v in r]
    print('字典数据路由：', dict_data)
    # 返回路由
    return dict_data


# 配置路由
def config_router():
    # 路由配置
    r = [
        (r'/list?', ConfigListHandler),
        (r'/detail/(\d+)', ConfigDetailHandler),
        (r'/add', ConfigAddHandler),
        (r'/update', ConfigUpdateHandler),
        (r'/delete/(\d+)', ConfigDeleteHandler),
    ]
    # 路由前缀
    config_r = "/config"
    # 路由数组
    config = [(config_r + v[0], v[1]) for v in r]
    print('配置路由：', config)
    # 返回路由
    return config


# 配置数据路由
def config_data_router():
    # 路由配置
    r = [
        (r'/list?', ConfigDataListHandler),
        (r'/detail/(\d+)', ConfigDataDetailHandler),
        (r'/add', ConfigDataAddHandler),
        (r'/update', ConfigDataUpdateHandler),
        (r'/delete/(\d+)', ConfigDataDeleteHandler),
        (r'/status', ConfigDataStatusHandler),
    ]
    # 路由前缀
    config_data_r = "/configdata"
    # 路由数组
    config_data = [(config_data_r + v[0], v[1]) for v in r]
    print('配置数据路由：', config_data)
    # 返回路由
    return config_data


# 网站配置路由
def config_web_router():
    # 路由配置
    r = [
        (r'/index', ConfigWebIndexHandler),
        (r'/save', ConfigWebSaveHandler),

    ]
    # 路由前缀
    config_web_r = "/configweb"
    # 路由数组
    config_web = [(config_web_r + v[0], v[1]) for v in r]
    print('网站配置路由：', config_web)
    # 返回路由
    return config_web


# 初始化路由
def init_routers():
    print('初始化路由')
    # 登录路由
    router.extend(login_router())
    # 主页路由
    router.extend(index_router())
    # 上传文件路由
    router.extend(upload_router())
    # 职级路由
    router.extend(level_router())
    # 岗位路由
    router.extend(position_router())
    # 部门路由
    router.extend(dept_router())
    # 角色路由
    router.extend(role_router())
    # 角色菜单路由
    router.extend(role_menu_router())
    # 菜单路由
    router.extend(menu_router())
    # 用户路由
    router.extend(user_router())
    # 城市路由
    router.extend(city_router())
    # 通知路由
    router.extend(notice_router())
    # 站点路由
    router.extend(item_router())
    # 栏目路由
    router.extend(item_cate_router())
    # 友链路由
    router.extend(link_router())
    # 广告位路由
    router.extend(ad_sort_router())
    # 广告路由
    router.extend(ad_router())
    # 会员等级路由
    router.extend(member_level_router())
    # 会员路由
    router.extend(member_router())
    # 字典路由
    router.extend(dict_router())
    # 字典数据路由
    router.extend(dict_data_router())
    # 配置路由
    router.extend(config_router())
    # 配置数据路由
    router.extend(config_data_router())
    # 网站配置路由
    router.extend(config_web_router())
    # 返回结果
    return router
